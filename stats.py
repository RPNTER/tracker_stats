#! /usr/bin/env python
import requests
import re
import yaml
import sys

CONFIG_FILE = "stats.yaml"

def load():
	global cfg
	global trackers

	cfg = yaml.load(open(CONFIG_FILE, "r"))
	trackers = dict([(k, v) for k, v in cfg["trackers"].iteritems()])

def show_stats(tn):
	t = trackers[tn]
	cookie = dict(map(lambda l : l.split("="), t["cookie"].split(";")))

	try:
		profile = requests.get(t["url"], headers={"User-Agent" : cfg["general"]["user-agent"]}, cookies=cookie).text
	except Exception,e:
		print e
		return	

	stats = re.findall(r""+t["regex"], profile)

	su = float(stats[0][0])
	suu = stats[0][1]
	sd = float(stats[1][0])
	sdu = stats[1][1]

	if suu != sdu:
		if suu[0] == "G" and sdu[0] == "M":
			sd = sd / 1000
			sdu = suu
		elif suu[0] == "T" and sdu[0] == "G":
			sd = sd / 1000
			sdu = suu
		if sdu[0] == "G" and suu[0] == "M":
			su = su / 1000
			suu = sdu
		elif sdu[0] == "T" and suu[0] == "G":
			su = su / 1000
			suu = sdu

	print "%s: %.2f%sB, %.2f%sB, %.2f%sB, %.2f" % (tn, su, suu, sd, sdu, su - sd, suu, su / sd)

def main():
	load()
	global trackers
        map(show_stats, filter(lambda x: trackers[x]["enabled"], (trackers if len(sys.argv) == 0 else sys.argv[1]).split(",")))

if __name__ == "__main__":
	main()
